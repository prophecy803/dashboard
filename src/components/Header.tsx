import React from 'react';
import styled from 'styled-components';
import { ReactComponent as Logo } from '../icons/logo.svg';
import { ReactComponent as Building } from '../icons/building.svg';
import { ReactComponent as List } from '../icons/list.svg';
import Sidebar from './Sidebar';

const Header = () => {
  return (
    <>
      <HeaderContainer>
        <SidebarWrapper>
          <Sidebar />
        </SidebarWrapper>
        <Logo />
        <Menu>
          <li>
            <Building />
            <div>A 가공 업체</div>
          </li>
          <Line />
          <li>
            <div>로그아웃</div>
          </li>
        </Menu>
      </HeaderContainer>
    </>
  );
};

const HeaderContainer = styled.header`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 1rem 1.5rem;
  background-color: ${props => props.theme.colors.primary};
  box-shadow: 0px 0px 2px rgba(0, 0, 0, 0.12), 0px 2px 2px rgba(0, 0, 0, 0.24);
  @media only screen and (max-width: 767px) {
    justify-content: flex-start;
  }
`;

const SidebarWrapper = styled.div`
  display: none;
  @media only screen and (max-width: 767px) {
    display: inline;
    padding-right: 1rem;
    cursor: pointer;
  }
`;

const Line = styled.div`
  width: 1px;
  height: 1rem;
  background-color: white;
`;

const Menu = styled.ul`
  color: white;
  display: flex;
  & > li {
    display: flex;
    list-style: none;
    padding: 0 10px 0 10px;
  }
  @media only screen and (max-width: 767px) {
    display: none;
  }
`;

export default Header;
