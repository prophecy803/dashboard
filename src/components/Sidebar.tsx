import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Button from '@material-ui/core/Button';
import { ReactComponent as Logo } from '../icons/drawerLogo.svg';
import { ReactComponent as Menu } from '../icons/list.svg';
import BusinessIcon from '@material-ui/icons/Business';

const useStyles = makeStyles({
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
});

export default function Sidebar() {
  const classes = useStyles();
  const [isOpen, setIsOpen] = React.useState(false);

  const toggleDrawer = (boolean: boolean) => {
    setIsOpen(boolean);
  };

  return (
    <div>
      <React.Fragment>
        <Button
          onClick={() => {
            toggleDrawer(true);
          }}>
          <Menu />
        </Button>
        <Drawer
          anchor="left"
          open={isOpen}
          onClose={() => {
            toggleDrawer(false);
          }}>
          <div
            className={clsx(classes.list)}
            role="presentation"
            onClick={() => {
              toggleDrawer(true);
            }}>
            <Logo />
            <Divider />
            <List>
              <ListItem button>
                <ListItemIcon>
                  <BusinessIcon />
                </ListItemIcon>
                <ListItemText primary="A 가공 업체" />
              </ListItem>
              <ListItem button>
                <ListItemText primary="로그아웃" />
              </ListItem>
            </List>
          </div>
        </Drawer>
      </React.Fragment>
    </div>
  );
}
