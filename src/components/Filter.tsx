import React, { useState, useEffect } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import Checkbox from '@material-ui/core/Checkbox';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemText from '@material-ui/core/ListItemText';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
  })
);

interface Props {
  options: string[];
  label: string;
  filterName: string;
  filter: {
    isConsulting: boolean;
    method: string[];
    material: string[];
  };
  setFilter: React.Dispatch<React.SetStateAction<any>>;
}

const Filter = (props: Props) => {
  const classes = useStyles();
  const [optionName, setOptionName] = useState<string[]>([]);

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setOptionName(event.target.value as string[]);
  };

  useEffect(() => {
    if (props.filterName === 'method') {
      props.setFilter({ ...props.filter, method: optionName });
    } else if (props.filterName === 'material') {
      props.setFilter({ ...props.filter, material: optionName });
    }
  }, [optionName]);

  return (
    <div>
      <FormControl variant="outlined" className={classes.formControl}>
        <InputLabel>{props.label}</InputLabel>
        <Select
          multiple
          value={optionName}
          onChange={handleChange}
          renderValue={selected =>
            `${props.label}(${(selected as string[]).length})`
          }>
          {props.options.map((val: any) => (
            <MenuItem key={val} value={val}>
              <Checkbox
                color="primary"
                checked={optionName.indexOf(val) > -1}
              />
              <ListItemText primary={val} />
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
};

export default Filter;
