import React, { useState, useEffect } from 'react';
import axios from 'axios';
import styled from 'styled-components';
import QuoteRequest from './QuoteRequest';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { ReactComponent as Refresh } from '../icons/refresh.svg';
import Filter from './Filter';
interface IRequest {
  id: number;
  title: string;
  client: string;
  due: string;
  count: number;
  amount: number;
  method: string[];
  material: string[];
  status: string;
}

interface IFilter {
  isConsulting: boolean;
  method: string[];
  material: string[];
}

const Main = () => {
  const [requests, setRequests] = useState<IRequest[]>([]);
  const [filter, setFilter] = useState<IFilter>({
    isConsulting: false,
    method: [],
    material: [],
  });
  let filteredData: IRequest[] = [];

  const resetFilter = () => {
    setFilter({ ...filter, isConsulting: false, method: [], material: [] });
  };

  const handleIsConsulting = (event: React.ChangeEvent<HTMLInputElement>) => {
    setFilter({ ...filter, isConsulting: !filter.isConsulting });
  };

  const getAllRequests = () =>
    axios(`${process.env.REACT_APP_URL}`).then(data => {
      setRequests(data.data);
      filteredData = data.data;
    });

  useEffect(() => {
    getAllRequests();
  }, []);

  function setIsFiltered() {
    if (
      !filter.isConsulting &&
      filter.method.length === 0 &&
      filter.material.length === 0
    ) {
      return false;
    }
    return true;
  }

  function FilterMethod(requests: IRequest) {
    if (filter.method.length === 0) return true;
    return requests.method.some(v => filter.method.includes(v));
  }

  function FilterMaterial(requests: IRequest) {
    if (filter.material.length === 0) return true;
    return requests.material.some(v => filter.material.includes(v));
  }

  function FilterIsConsulting(requests: IRequest) {
    if (!filter.isConsulting) return true;
    return requests.status === '상담중';
  }

  const callbacks = [FilterMethod, FilterMaterial, FilterIsConsulting];

  const FilteredRequests = () => {
    filteredData = setIsFiltered()
      ? callbacks.reduce((acc: IRequest[], fn) => {
          return acc.filter(fn);
        }, requests)
      : requests;

    return (
      <>
        {filteredData.length > 0 ? (
          filteredData.map(request => {
            return <QuoteRequest key={request.id} {...request} />;
          })
        ) : (
          <EmptyContainer>조건에 맞는 견적요청이 없습니다.</EmptyContainer>
        )}
      </>
    );
  };

  return (
    <MainContainer>
      <div>
        <Foo>
          <TitleContainer>
            <MainTitle>들어온 요청</MainTitle>
            <SubTitle>파트너님에게 딱 맞는 요청서를 찾아보세요.</SubTitle>
          </TitleContainer>
          <FiltersWrapper>
            <FilterContainer>
              <Filter
                options={Array.from(
                  new Set(
                    requests.reduce((acc, cur) => {
                      return acc.concat(cur.method);
                    }, [] as string[])
                  )
                )}
                label="가공방식"
                filterName="method"
                filter={filter}
                setFilter={setFilter}
              />
              <Filter
                options={Array.from(
                  new Set(
                    requests.reduce((acc, cur) => {
                      return acc.concat(cur.material);
                    }, [] as string[])
                  )
                )}
                label="재료"
                filterName="material"
                filter={filter}
                setFilter={setFilter}
              />
              <ResetBtn onClick={resetFilter}>
                <Refresh />
                필터링 리셋
              </ResetBtn>
            </FilterContainer>
            <SwitchWrapper>
              <FormControlLabel
                control={
                  <Switch
                    onChange={handleIsConsulting}
                    name="checked"
                    color="primary"
                  />
                }
                label="상담 중인 요청만 보기"
              />
            </SwitchWrapper>
          </FiltersWrapper>
        </Foo>
        <RequestsContainer>
          <FilteredRequests />
        </RequestsContainer>
      </div>
    </MainContainer>
  );
};

const MainContainer = styled.div`
  padding: 3rem;
  display: flex;
  justify-content: center;
  @media only screen and (max-width: 767px) {
    flex-direction: column;
  }
`;

const Foo = styled.div`
  @media only screen and (min-width: 767px) {
    min-width: 1130px;
  }
  @media only screen and (max-width: 767px) {
    width: 100vw;
  }
`;

const TitleContainer = styled.div`
  margin: 0.8rem;
`;

const MainTitle = styled.div`
  font-size: 20px;
  font-weight: 700;
  line-height: 32px;
`;

const SubTitle = styled.div`
  font-size: 16px;
  line-height: 24px;
`;

const FiltersWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  @media only screen and (max-width: 767px) {
    width: 100%;
    flex-direction: column;
    justify-content: flex-start;
  }
`;

const ResetBtn = styled.div`
  display: flex;
  align-items: center;
  color: ${props => props.theme.colors.primaryAccent};
  cursor: pointer;
  @media only screen and (max-width: 767px) {
    display: none;
  }
`;

const SwitchWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin: 0 2rem;
  & > p {
    padding-bottom: 0;
  }
`;

const FilterContainer = styled.div`
  display: flex;
  align-items: center;
`;

const RequestsContainer = styled.div`
  display: flex;
  justify-content: flex-start;
  flex-wrap: wrap;
  height: 70vh;
  max-width: 1130px;
  overflow-y: auto;
`;

const EmptyContainer = styled.div`
  ${props => props.theme.alignCenter}
  width: 100%;
  height: 7rem;
  margin: 0 0.5rem;
  color: ${props => props.theme.colors.graySub};
  border: ${props => `1px solid ${props.theme.colors.base}`};
  overflow-y: hidden;
`;
export default Main;
