import React from 'react';
import styled from 'styled-components';

const QuoteRequest = ({ ...props }) => {
  return (
    <RequestContainer>
      <GeneralInfo>
        <Title>{props.title}</Title>
        <Client>{props.client}</Client>
        <Due>{props.due}까지 납기</Due>
        <Line />
      </GeneralInfo>
      {props.status === '상담중' && <Badge>상담중</Badge>}
      <Column>
        <Label>
          <p>도면개수</p>
          <p>총 수량</p>
          <p>가공방식</p>
          <p>재료</p>
        </Label>
        <Contents>
          <p>{props.count}</p>
          <p>{props.amount}</p>
          <p>{props.method.join(', ')}</p>
          <p>{props.material.join(', ')}</p>
        </Contents>
      </Column>
      <ButtonContainer>
        <DetailBtn>요청 내역 보기</DetailBtn>
        <ChatBtn>채팅하기</ChatBtn>
      </ButtonContainer>
    </RequestContainer>
  );
};

const RequestContainer = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
  margin: 0.8rem;
  padding: 1.2rem;
  width: 346px;
  height: 356px;
  border: ${props => `2px solid ${props.theme.colors.base}`};
  border-radius: 4px;
  &:hover {
    border: ${props => `2px solid ${props.theme.colors.primaryAccent}`};
    border-radius: 4px;
  }
`;

const GeneralInfo = styled.div``;

const Title = styled.p`
  font-size: 16px;
  font-weight: 700;
`;

const Client = styled.p`
  font-weight: 500;
`;

const Due = styled.div`
  color: ${props => props.theme.colors.graySub};
  padding-top: 1rem;
`;

const Badge = styled.div`
  ${props => props.theme.alignCenter}
  position: absolute;
  right: 1rem;
  top: 2rem;
  padding: 5px;
  font-size: 12px;
  color: ${props => props.theme.colors.warning};
  border: ${props => `1px solid ${props.theme.colors.warning}`};
  border-radius: 12px;
`;

const Line = styled.div`
  height: 1px;
  width: 100%;
  margin-top: 1.2rem;
  border: ${props => `1px solid ${props.theme.colors.base}`};
`;

const Column = styled.div`
  display: flex;
  padding: 0.4rem 0;
`;

const Label = styled.div`
  width: 30%;
`;

const Contents = styled.div`
  margin: 0;
  font-weight: 700;
  color: ${props => props.theme.colors.gray};
`;

const ButtonContainer = styled.div`
  display: flex;
  & > button {
    margin-right: 12px;
    padding: 6px 12px;
    font-weight: 500;
    line-height: 20px;
    border-radius: 4px;
  }
`;

const DetailBtn = styled.button`
  color: white;
  background-color: ${props => props.theme.colors.primaryAccent};
  border: none;
`;

const ChatBtn = styled.button`
  background-color: white;
  color: ${props => props.theme.colors.primaryAccent};
  border: ${props => `1px solid ${props.theme.colors.primaryAccent}`};
`;

export default QuoteRequest;
