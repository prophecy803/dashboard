import { DefaultTheme } from 'styled-components';

const theme: DefaultTheme = {
  colors: {
    primary: '#1565C0',
    primaryAccent: '#2196F3',
    gray: '#323D45',
    graySub: '#939FA5',
    warning: '#FFA000',
    base: '#e5e5e5',
  },
  alignCenter: `
    display: flex;
    align-items: center;
    justify-content: center;
  `,
};

export default theme;
